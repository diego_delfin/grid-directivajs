(function(){
	'use strict';

	angular
		.module('myApp',['ngRoute'])
		.config(appConfig)
		.service('MyService', MyService)
		.directive('miItem' , miItem)
		.directive('miDirectiva', miDirectiva);



	function appConfig ($routeProvider) {
		$routeProvider
			.when('/', {
				template : '<mi-directiva></mi-directiva>'
			});
	}


 	
 	function miDirectiva () {
 		return {
 			scope : {},
 			controller : function(MyService) {
 				this.numero = 0 ;
 				this.productos = MyService.getData();
 				this.addTable = function () {
 					this.numero = this.numero + 1;
 					console.log('num', this.numero);
 				}
 			},
  			controllerAs : 'vm',
  			template:[
  				'<h1>Listado de Productos</h1>',
  				'<button class="btn btn-primary btn-xs" ng-click="vm.addTable();">+</button>',
  				'<ul>',
  				  '<mi-item ng-repeat="producto in vm.productos" data="producto"> ',
  				  '</mi-item>',
  				 '</ul>'
  				 ].join('')
 		}
 	}
  	
  	function miItem () {
  		return {
  			scope : {
  				data : '='
  			},
  			template : [
  			       '<li>',
  			        '<strong> {{ data.titulo }} </strong>:',
  			          '{{ data.precio | currency }}',
  			       '</li>'
  			].join('')
  		}
  	}



	function MyService() {
		return {
			getData : getData
		}
	}

	function getData() {
		var datos = [
			{ titulo : "Producto 1", precio :2 },
			{ titulo : "Producto 2", precio : 2.4},
			{ titulo : "Producto 3", precio :4 },
			{ titulo : "Producto 4", precio : 5},
			{ titulo : "Producto 5", precio : 3.4}
		];

		return datos;
	}

})();